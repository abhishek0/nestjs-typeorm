import { Controller, Get, Post, Put, Delete, Param, Body, ParseIntPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { User } from './user.entity';

@ApiTags('Users')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/hello')
  getHello(): string {
    return this.appService.getHello();
  }

  @Get()
  getAll(): Promise<User[]> {
  	return this.appService.getAll();
  }

  @Get(':userId')
  getUserById(@Param('userId', ParseIntPipe) userId: number): Promise<User> {
  	return this.appService.getUserById(userId);
  }

  @Post()
  createUser(@Body() body: User): Promise<User> {
  	return this.appService.createUser(body.name, body.age);
  }

  @Put(':userId')
  updateUser(@Param('userId', ParseIntPipe) userId: number, @Body() body) {
  	return this.appService.updateUser(userId, body.name, body.age);
  }

  @Delete(':userId')
  deleteUser(@Param('userId', ParseIntPipe) userId: number) {
  	return this.appService.deleteUser(userId);
  }
}
