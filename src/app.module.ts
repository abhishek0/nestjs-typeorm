import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from './user.entity';
import { PetController } from './pet/pet.controller';
import { PetService } from './pet/pet.service';
import { PetModule } from './pet/pet.module';
import { Pet } from './pet.entity';
import config from '../ormconfig';

@Module({
  imports: [
  	TypeOrmModule.forRoot(config),
  	TypeOrmModule.forFeature([User, Pet]),
  	PetModule,
  ],
  controllers: [AppController, PetController],
  providers: [AppService, PetService],
})
export class AppModule {}
