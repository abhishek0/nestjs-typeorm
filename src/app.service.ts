import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class AppService {
	constructor(@InjectRepository(User) private usersRepository: Repository<User>) {}

	getHello(): string {
		return 'Hello World!';
	}

	getAll(): Promise<User[]> {
		return this.usersRepository.find({
			relations: ['pets']
		})
	}

	async getUserById(id: number): Promise<User> {
		try {
			const user: User = await this.usersRepository.findOneOrFail(id);
			return user;
		} catch(err) {
			throw err;
		}
	}

	createUser(name: string, age: number): Promise<User> {
		const newUser = this.usersRepository.create({name, age});
		return this.usersRepository.save(newUser);
	}

	async updateUser(id: number, name: string, age: number): Promise<User> {
		const user = await this.getUserById(id);
		user.name = name || user.name;
		user.age  = age  || user.age;
		return this.usersRepository.save(user);
	}

	async deleteUser(id: number): Promise<User> {
		const user = await this.getUserById(id);
		return this.usersRepository.remove(user);
	}
}
