import { Controller, Get, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PetService } from './pet.service';

@ApiTags('Pets')
@Controller('pet/pet')
export class PetController {
	constructor(private readonly petService: PetService) {}

	@Get()
	findAll() {
		return 'All pets!!!\n';
	}

	@Get(':petId')
	findPetById() {
		return 'Pet with id falana\n';
	}

	@Post()
	createPet() {
		return 'Pet created\n';
	}
}
