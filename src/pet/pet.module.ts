import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PetController } from './pet.controller';
import { PetService } from './pet.service';
import { Pet } from '../pet.entity';

@Module({
	imports: [TypeOrmModule.forFeature([Pet])],
	controllers: [PetController],
	providers: [PetService]
})
export class PetModule {}
