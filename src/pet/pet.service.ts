import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Pet } from '../pet.entity';

@Injectable()
export class PetService {
	constructor(@InjectRepository(Pet) private petRepository: Repository<Pet>) {}

	findAll() {

	}

	findPetById(petId: number) {

	}

	createPet(name: string, age: number) {

	}
}
