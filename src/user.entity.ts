import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Pet } from './pet.entity';

@Entity()
export class User {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	id: number;

	@ApiProperty()
	@Column()
	name: string;

	@ApiProperty()
	@Column()
	age: number;

	@OneToMany(type => Pet, pet => pet.owner)
	pets: Pet[];
}